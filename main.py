import sys
from ec.utilisateurs import user
from ec.extraits.fichier import Fichier
from PyQt6.QtGui import QAction
from PyQt6.QtWidgets import (
    QMainWindow, QApplication,
    QMenuBar, QToolBar, QWidget,
    QWidgetAction, QFileDialog,
    QDialog, QMdiSubWindow, QMdiArea, QLabel, QTabWidget
)
from PyQt6 import uic

class Interface(QMainWindow):
    def __init__(self):
        super(Interface, self).__init__()

        # Chargement du fichier .ui
        self.ui = uic.loadUi("ui/interface.ui", self)

        # --- Définition et paramétrage des composants
        self.menu = self.findChild(QMenuBar, "menubar")
        self.actionQuitter.triggered.connect(self.terminer)
        self.fichier = self.actionOuvrir.triggered.connect(self.openFile)
        self.mdi = self.findChild(QMdiArea, "mdiArea")

    # Mettre Fin à l'execution de l'application
    def terminer(self):
        exit(0)

    # Ouvrir une fichier (*.pdf, *.docx, *.doc)
    def openFile(self):
        fname = QFileDialog.getOpenFileName(
            self,
            "Open File",
            "",
            "All Files (*);; Fichier PDF (*.pdf);; Office 2006 (*.doc);; Office 2016 (*.docx)",
        )
        # Recupération du fichier ouvert
        fichier = Fichier(fname[0])

        # Chargement de la fenêtre d'affichage des informations sur le fichier ouvert
        fichierFrm = uic.loadUi("ui/fichier.ui")
        fichierFrm.setWindowTitle("Fichier")

        #fichierFrm.contenu_fichier.setText(fichier.file_content + "\n\n" + str(fichier.file_meta))


        #fichierMdi.setWindowTitle("Fichier extrait")
        self.mdi.addSubWindow(fichierFrm)
        fichierFrm.showMaximized()
        #print(fichier.file_content.strip())
        return fichier


app = QApplication(sys.argv)
# Create a Qt widget, which will be our window.
interface = Interface()
interface.show()  # IMPORTANT!!!!! Windows are hidden by default.
app.exec()