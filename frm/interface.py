from PyQt6 import uic
from PyQt6.QtGui import QAction
from PyQt6.QtWidgets import QMainWindow, QFileDialog, QMdiArea

from ec.extraits.fichier import Fichier


class MainWindow(QMainWindow):
    def __init__(self, user):
        super(MainWindow, self).__init__()

        # Importation interface utilisasteur associée
        self.ui = uic.loadUi("ui/interface.ui", self)

        # Initialisation des variables
        self.user = user
        self.ui.setWindowTitle(self.user.prenom)

        self.actionQuitter = self.ui.findChild(QAction, "actionQuitter")
        self.actionQuitter.triggered.connect(self.fermer)

        self.actionOuvrir = self.ui.findChild(QAction, "actionOuvrir")
        self.actionOuvrir.triggered.connect(self.openFile)

        self.mdi = self.ui.findChild(QMdiArea, "mdiArea")

    def fermer(self):
        exit(0)

    def openFile(self):
        fname = QFileDialog.getOpenFileName(
            self,
            "Open File",
            "",
            "All Files (*);; Fichier PDF (*.pdf);; Office 2006 (*.doc);; Office 2016 (*.docx)",
        )
        # Recupération du fichier ouvert
        fichier = Fichier(fname[0])

        # Chargement de la fenêtre d'affichage des informations sur le fichier ouvert
        fichierFrm = uic.loadUi("ui/fichier.ui")
        fichierFrm.setWindowTitle("Fichier")



        #fichierMdi.setWindowTitle("Fichier extrait")
        self.mdi.addSubWindow(fichierFrm)
        fichierFrm.showMaximized()
