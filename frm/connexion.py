from PyQt6 import uic
from PyQt6.QtWidgets import QDialog, QMessageBox, QLineEdit
from ec.utilisateurs.user import User
from frm.interface import MainWindow
from settings import Session, engine

class Connexion(QDialog):
    def __init__(self):
        super(Connexion, self).__init__()

        # Import de l'interface utilisateur
        self.ui = uic.loadUi("ui/login.ui", self)
        # Initialisation des variables
        self.username = self.ui.led_username
        self.password = self.ui.led_password
        self.password.setEchoMode(QLineEdit.EchoMode.Password)
        self.btn_quit = self.ui.btn_quit
        self.btn_connexion = self.ui.btn_connexion

        # Initialisation des évènements
        self.btn_quit.clicked.connect(self.fermer)
        self.btn_connexion.clicked.connect(self.connexion)

    def fermer(self):
        exit(0)

    def connexion(self):
        local_session = Session(bind=engine)
        u = self.username.text()
        p = self.password.text()
        try:
            user = local_session.query(User).filter(User.mdp==str(p)).filter(User.username==str(u)).one()
            win = MainWindow(user)
            win.showMaximized()
            self.hide()
        except Exception as e:
            dlg = QMessageBox(self)
            dlg.setWindowTitle("Connexion")
            dlg.setText(f"Echec de la connexion : \n {e}")
            dlg.exec()