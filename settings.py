import os
from sqlalchemy.orm import declarative_base
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker


BASE_DIR = os.path.dirname(os.path.realpath(__file__))
lien_connexion = "sqlite:///" + os.path.join(BASE_DIR,'eciv.db')
Base = declarative_base()
engine = create_engine(lien_connexion, echo=True)
Session = sessionmaker()