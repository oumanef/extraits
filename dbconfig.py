from ec.extraits.zone import Zone
from ec.extraits.pays import Pays
from ec.extraits.fichier import Fichier
from ec.extraits.extrait import Extrait
from ec.utilisateurs.user import User
from ec import Base, engine

Base.metadata.create_all(engine)