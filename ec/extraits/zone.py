from sqlalchemy import Column, ForeignKey, String, CHAR, Integer
from ec import Base


class Zone(Base):
    __tablename__ = "zones"

    id = Column("id", Integer, primary_key=True)
    nomZone = Column("zone", String)

    def __init__(self, nomZone):
        self.nomZone = nomZone