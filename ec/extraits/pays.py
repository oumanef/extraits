from sqlalchemy import Column, ForeignKey, String, CHAR, Integer
from ec import Base
from ec.extraits.zone import Zone


class Pays(Base):
    __tablename__ = "pays"

    id = Column("id", Integer, primary_key=True)
    pays = Column("pays", String)
    capitale = Column("capitale", String)
    iso2 = Column("iso2", String(2))
    iso3 = Column("iso3", String(3))
    iso_num = Column("iso_num", String)
    zone = Column("zone", ForeignKey("zones.id"))

    def __init__(self, pays, capitale, iso2, iso3, iso_num, zone):
        self.pays = pays
        self.capitale = capitale
        self.iso2 = iso2
        self.iso3 = iso3
        self.iso_num = iso_num
        self.zone = zone

    def __repr__(self):
        return f"({self.pays})"