import tika
from tika import parser
from tika import parser


class Fichier(object):
    def __init__(self, fichier):
        tika.initVM()
        try:
            parsed = parser.from_file(fichier, 'http://localhost:9998/tika')
            self.file_meta = parsed["metadata"]
            self.file_content = parsed["content"]
        except Exception as e:
            print(e)
