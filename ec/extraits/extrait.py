import sys
from sqlalchemy import Column, ForeignKey, CHAR, Integer, String, DateTime
from ec.extraits.pays import Pays
from ec import Base


class Extrait(Base):
    __tablename__ = "extraits"

    id = Column("id", Integer, primary_key=True)
    annee = Column("annee", Integer)
    numReg = Column("numreg", Integer)
    dateN = Column("date_n", DateTime)
    heureN = Column("heure_n", Integer)
    minuteN = Column("minute_n", Integer, default=0)
    lieuN = Column("lieu_n", String)
    sexe = Column("sexe", CHAR(1))
    prenom = Column("prenom", String)
    nom = Column("nom", String)

    pere = Column("pere", String)
    pMere = Column("pmere", String)
    nMere = Column("nmere", String)
    paysN = Column("pays_n", ForeignKey("pays.id"))

    def __init__(self, annee, numReg, dateN, heureN, minuteN, lieuN, sexe, prenom, nom, pere, pMere, nMere, paysN):
        self.annee = annee
        self.numReg = numReg
        self.dateN = dateN
        self.heureN = heureN
        self.minuteN = minuteN
        self.lieuN = lieuN
        self.sexe = sexe
        self.prenom = prenom
        self.nom = nom
        self.pere = pere
        self.pMere = pMere
        self.nMere = nMere
        self.paysN = paysN

    def __repr__(self):
        return f"{self.prenom} {self.nom}, né(e) le {self.dateN} ({self.paysN}) de {self.pere} et de {self.pMere} {self.nMere}"