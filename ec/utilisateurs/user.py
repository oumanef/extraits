from sqlalchemy import ForeignKey, Column, String, Integer, CHAR, Boolean
from ec import Base

class User(Base):
    __tablename__ = "users"

    id = Column("id", Integer, primary_key=True)
    nom = Column("nom", String)
    prenom = Column("prenom", String)
    username = Column("username", String)
    mdp = Column("mdp", String)
    email = Column("mail", String)
    is_superuser = Column("is_superuser", Boolean, default=False)
    is_staff = Column("is_staff", Boolean, default=False)
    is_active = Column("is_active", Boolean, default=True)

    def __init__(self, username, mdp, prenom, nom, email, is_superuser=False, is_staff=False, is_active=True):
        self.nom = nom
        self.prenom = prenom
        self.username = username
        self.mdp = mdp
        self.email = email
        self.is_active = is_active
        self.is_staff = is_staff
        self.is_superuser = is_superuser

    def __repr__(self):
        return f"{self.prenom} {self.nom} {self.is_active}"