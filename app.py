from PyQt6.QtWidgets import QApplication
from frm.connexion import Connexion
from sqlalchemy.orm import sessionmaker
import sys, os
from settings import *


Session = sessionmaker()
def main():
    app = QApplication(sys.argv)
    login = Connexion()
    login.show()
    app.exec()


if __name__ == '__main__':
    main()